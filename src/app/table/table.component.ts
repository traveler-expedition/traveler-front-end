import { OnInit, Component } from '@angular/core';
import { NotasService } from '../notas.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css'],
})
export class TableComponent implements OnInit {
  notas: any = [];
  displayedColumns = [
    'cnpj',
    'numero_nota',
    'data_emissao',
    'pedido_loja',
    'chave_acesso',
    'status',
    'enviar',
    'link_danfe',
  ];

  constructor(private NotaSrv: NotasService) {}

  async ngOnInit() {
    this.notas = await this.NotaSrv.listar();
    console.log(this.notas);
  }
  async excluir(nota: any) {
    if (confirm('Deseja realmente excluir?')) {
      try {
        await this.NotaSrv.atualizar(nota);
        this.ngOnInit();
      } catch (erro) {
        console.log(erro);
      }
    }
  }
}
